import { Component, OnInit } from '@angular/core';
import { StockPriceService } from '../stock-price.service';

@Component({
  selector: 'app-price-table',
  templateUrl: './price-table.component.html',
  styleUrls: ['./price-table.component.css']
})
export class PriceTableComponent implements OnInit {

  public priceData: any;

  constructor(private stockPriceService: StockPriceService) { }

  ngOnInit(): void {
    this.stockPriceService.getPrice().subscribe( (data) => {
        console.log(data);
        this.priceData = data;
    });
  }

}
