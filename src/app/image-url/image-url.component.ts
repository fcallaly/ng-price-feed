import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-url',
  templateUrl: './image-url.component.html',
  styleUrls: ['./image-url.component.css']
})
export class ImageUrlComponent implements OnInit {
  public imageUrl: string;

  @Input() someParameter: string;

  constructor() { }

  ngOnInit(): void {
  }

}
