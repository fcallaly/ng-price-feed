import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ImageUrlComponent } from './image-url/image-url.component';
import { PriceTableComponent } from './price-table/price-table.component';

@NgModule({
  declarations: [
    AppComponent,
    ImageUrlComponent,
    PriceTableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
