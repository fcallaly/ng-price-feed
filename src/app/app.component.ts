import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'My New Title';
  private counter = 0;
  public myLabel: string;

  public myFunction() {
    this.title = "MyFunction change the title: " + this.counter;

    console.log("This is myFunction executing, count: " + this.counter);

    this.counter = this.counter + 1;
  }

}
