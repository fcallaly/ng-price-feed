import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StockPriceService {

  constructor(private httpClient: HttpClient) { }

  public getPrice(): Observable<any> {
    return this.httpClient.get<any>(
      environment.priceUrl + "?ticker=C&num_days=3"
    )
  }
}
